<?php

namespace App\Http\Controllers;


use App\Account;
use App\Exports\UniverExport;
use App\Profile;
use App\Univer;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Http\UploadedFile;
use App\Exports\ProfilesExport;
use Illuminate\Support\Facades\Input;
use InstagramAPI\Instagram;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class InstagramWorkerController
 * @package App\Http\Controllers
 */
class InstagramWorkerController extends Controller
{

    private $ig;
    private $login;
    private $password;
    private $twoFactorIdentifier;


    /**
     * InstagramWorkerController constructor.
     */
    public function __construct() {}

    public function setInstagramInstance()
    {
        \InstagramAPI\Instagram::$allowDangerousWebUsageAtMyOwnRisk = true;

        $this->ig  = new Instagram(false,false,
            [
                'storage'    => env('DB_CONNECTION'),
                'dbhost'     => env('DB_HOST'),
                'dbname'     => env('DB_DATABASE'),
                'dbusername' => env('DB_USERNAME'),
                'dbpassword' => env('DB_PASSWORD'),
            ]);
    }

    public function start()
    {
        try {
            $mode  = Input::get('mode');

            if ($mode == 'college') {
                Artisan::call('ig:start', [ '--mode' => 'college']);
            } else {
                Artisan::call('ig:start');
            }

            return new JsonResponse(['result' => 'success']);
        } catch (\Exception $exception) {
            return new JsonResponse(['result' => 'error', 'error' => $exception->getMessage()]);
        }

    }

    public function stop()
    {
        try {
            Artisan::call('ig:stop');
            return new JsonResponse(['result' => 'success']);
        } catch (\Exception $exception) {
            return new JsonResponse(['result' => 'error', 'error' => $exception->getMessage()]);
        }
    }

    public function resetProfiles()
    {
        try {
            Artisan::call('ig:clear:profiles');
            return new JsonResponse(['result' => 'success']);
        } catch (\Exception $exception) {
            return new JsonResponse(['result' => 'error', 'error' => $exception->getMessage()]);
        }
    }

    public function import(Request $request)
    {
        $data = $request->all();

        if (!empty($data['file'])) {
            $localFolderName = 'import';

            /** @var UploadedFile $importedFile */
            $importedFile = $data['file'];

            $file = $importedFile->getClientOriginalName();

            $fileName = Carbon::now()->timestamp. rand(). $file ;

            $importedFile->move(public_path($localFolderName), $fileName);

            $fileName = public_path($localFolderName). '/' . $fileName;

            if (isset($data['mode']) && $data['mode'] == 'college') {
                Artisan::call('ig:college:import', [
                    '--path' => $fileName
                ]);
            } else {
                Artisan::call('ig:import', [
                    '--path' => $fileName
                ]);
            }

            return new JsonResponse(['import' => 'success']);
        }
    }

    public function export()
    {
        $mode  = Input::get('mode');

        $fileName = Carbon::now()->timestamp. rand();

        $from = null;

        if ($mode == 'college') {
            $file = Excel::download(new UniverExport(), $fileName, \Maatwebsite\Excel\Excel::XLSX);
        } else {
            $file = Excel::download(
                new ProfilesExport($from), $fileName,
                \Maatwebsite\Excel\Excel::XLSX
            );
        }


        return $file;
    }

    public function addAccount(Request $request)
    {
        $data = $request->all();

        $this->setInstagramInstance();

        try {
            if (isset($data['login']) && isset($data['password'])) {
                $loginResponse = $this->ig->login($data['login'], $data['password']);

                if (!is_null($loginResponse) && $loginResponse->isTwoFactorRequired()) {
                    $this->twoFactorIdentifier = $loginResponse->getTwoFactorInfo()->getTwoFactorIdentifier();
                    $this->login = $data['login'];
                    $this->password = $data['password'];

                    return new JsonResponse(['result' => 'verification']);
                } else {
                    $account = Account::create(
                        [
                            'username' => $data['login'],
                            'password' => $data['password'],
                            'status' => 0
                        ]);

                    return new JsonResponse(['result' => 'success', 'account' => $account]);
                }
            } else {
                return new JsonResponse(['result' => 'error', 'error' => 'Login or password not fill']);
            }
        } catch (\Exception $exception) {
            return new JsonResponse(['result' => 'error', 'error' => $exception->getMessage()]);
        }


    }

    public function getAccounts()
    {
        $accounts = Account::all();

        return new JsonResponse(['accounts' => $accounts]);
    }

    public function finishVerification(Request $request)
    {
        $data = $request->only('code');

        $verificationCode = trim($data['code']);

        $this->ig->finishTwoFactorLogin($this->login, $this->password, $this->twoFactorIdentifier, $verificationCode);

        return new  JsonResponse(['result' => 'success']);
    }

    public function addProfile(Request $request)
    {
        $data = $request->only('username');

        if (isset($data['username'])) {

            if ($data['username'][0] === '@') {
                $data['username'] = substr($data['username'], 1);
            }

            Profile::create(['username' => $data['username'] ]);
        }
    }

    public function addCollege(Request $request)
    {
        $data = $request->only('college');

        if (isset($data['college'])) {

            if ($data['college'][0] === '@') {
                $data['college'] = substr($data['college'], 1);
            }

            Univer::create([ 'name' => $data['college'] ]);
        }
    }
}
