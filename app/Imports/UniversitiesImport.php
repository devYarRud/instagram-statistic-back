<?php

namespace App\Imports;

use App\Univer;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Validators\Failure;

class UniversitiesImport implements ToModel, WithValidation,  SkipsOnFailure
{

    use Importable, SkipsFailures;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Univer([
            'name' => $this->formattedCollegeName($row[0]),
        ]);
    }

    /**
     * @param $name
     * @return bool|string
     */
    public function formattedCollegeName($name)
    {
        if ($name[0] === '@') {
            $name = substr($name, 1);
        }

        return $name;
    }

    /**
     * @param Failure[] $failures
     */
    public function onFailure(Failure ...$failures)
    {
        // Handle the failures how you'd like.
    }

    public function rules(): array
    {

        return [
            '0' => 'required|unique:univers,name',
        ];
    }
}
