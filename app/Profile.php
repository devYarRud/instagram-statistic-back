<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    const STATUS_NEW = 0;
    const STATUS_ON_PROCESS = 1;
    const STATUS_CHECK = 2;
    const STATUS_UNCHECK = 3;
    const STATUS_USER_NOT_FOUND = 4;
    const STATUS_CHECK_WITH_FOLLOWERS = 5;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'status',
        'is_private', 'followers',
        'like_average', 'parent_id',
        'average_engagement', 'univer_id',
        'media_id', 'following', 'comment_average',
        'comment_average_engagement', 'public_email'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function college()
    {
        return $this->belongsTo('App\Univer','univer_id');
    }
}
