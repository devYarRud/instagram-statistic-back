<?php

namespace App\Exports;

use App\Profile;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithMapping;

class UniverProfilesInstaPageExport implements FromQuery, WithMapping, WithHeadings, WithTitle
{
    use Exportable;


    public function __construct()
    {
//        $this->date = $date;
    }

    public function query()
    {
        return Profile::query()
            ->where('univer_id', '!=', null)
            ->where('status', Profile::STATUS_CHECK)
            ->where('media_id', '=', null);
    }

    /**
     * @var Profile $profile
     */
    public function map($profile): array
    {
        return [
            $profile->college->name,
            $profile->username,
            $profile->followers,
            $profile->following,
            $profile->average_engagement,
            $profile->is_private,
            $profile->public_email,
        ];
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'College',
            'Username',
            'Followers',
            'Following',
            'Average Engagement',
            'Is private',
            'Email',
        ];
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'College page';
    }
}
