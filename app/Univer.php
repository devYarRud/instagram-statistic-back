<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Univer extends Model
{
    const STATUS_NEW = 0;
    const STATUS_ON_PROCESS = 1;
    const STATUS_CHECK = 2;
    const MAX_POSTS_COUNT = 50;
    const MAX_LIKERS_COMMENTS_COUNT = 50;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'status'
    ];

    public $timestamps = false;
}
