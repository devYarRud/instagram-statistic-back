<?php

namespace App\Console\Commands;


use App\Account;
use App\Jobs\ProcessMain;
use App\Jobs\ProcessUniver;
use App\Univer;
use Doctrine\Common\Collections\ArrayCollection;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Log;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use Supervisor\Configuration\Configuration;
use Supervisor\Configuration\Exception\WriterException;
use Supervisor\Configuration\Loader;
use Supervisor\Configuration\Loader\IniFileLoader;
use Supervisor\Configuration\Section;
use Supervisor\Configuration\Writer;
use Supervisor\Configuration\Writer\IniFileWriter;
use Supervisor\Exception\Fault;
use Supervisor\Exception\Fault\BadName;
use Supervisor\Supervisor;
use Supervisor\Connector\XmlRpc;
use fXmlRpc\Client;
use fXmlRpc\Transport\HttpAdapterTransport;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;
use \Http\Message\MessageFactory\DiactorosMessageFactory as MessageFactory;

/**
 * Class StartCommand
 * @package App\Console\Commands
 */
class StartCommand extends Command
{
    const FILESYSTEM_ROOT = '/';
    const SUPERVISOR_CONFIG_FILE = '/etc/supervisor/supervisord.conf';
    const SUPERVISOR_PROGRAM_NAME = 'igmp-workers';
    const SUPERVISOR_PROGRAM_COMMAND = 'queue:listen --timeout=0';

    /**
     * @var Collection|Account[] $accounts
     */
    protected $accounts;

    /**
     * @var int
     */
    protected $accountsCount = 0;

    /**
     * @var Configuration|null
     */
    protected $supervisorConfiguration = null;

    /**
     * @var Supervisor|null
     */
    protected $supervisorInstance = null;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ig:start {--mode=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->loadAccounts();

        $this->prepareSupervisorConfiguration();

        if (!$this->supervisorConfiguration instanceof Configuration) {
            $this->error('supervisorConfiguration is null');
            return;
        }

        $this->supervisorConnection();

        if (!$this->supervisorInstance instanceof Supervisor) {
            $this->error('supervisorInstance is null');
            return;
        }

        $this->supervisorInstance->restart();

        sleep(3);

        $mode = $this->option('mode');

        foreach ($this->accounts->getIterator() as $account) {
            if (isset($mode) && $mode == 'college') {
                try {
                    $universWithStatusInProcess = Univer::where('status', Univer::STATUS_ON_PROCESS)->get();
                    foreach ($universWithStatusInProcess as $univer) {
                        $univer->status = Univer::STATUS_NEW;
                        $univer->save();
                    }
                } catch (\Exception $exception) {
                    Log::error($exception->getMessage());
                }
                ProcessUniver::dispatch($account);
            } else {
                ProcessMain::dispatch($account);
            }
        }

        $this->info('Started');
    }

    /**
     * Load accounts from DB
     */
    protected function loadAccounts(): void
    {
        $this->accounts = Account::where('status', Account::STATUS_FREE)->get();

        $this->accountsCount = $this->accounts->count();
    }

    /**
     * Some info
     */
    protected function prepareSupervisorConfiguration(): void
    {
        /** @var Configuration $configuration */
        $configuration = $this->getReader()->load();

        $program = $this->foundProgram($configuration);

        if ($program instanceof Section\Program) {
            $configuration->removeSection(
                $program->getName()
            );

            $this->info('Remove previous program');
        }

        $configuration->addSection(
            $this->createProgram()
        );

        try {
            $this->getWriter()->write($configuration);

            $this->supervisorConfiguration = $configuration;

            $this->info('Supervisor program written successfully');
        } catch (WriterException $exception) {
            $this->error('Supervisor program error:' . $exception->getMessage());

            return;
        }
    }

    /**
     * @param Configuration $configuration
     * @return Section\Program|false
     */
    protected function foundProgram(Configuration $configuration)
    {
        $sections = new ArrayCollection($configuration->getSections());
        return $sections->filter(function (Section $section) {
            return
                $section instanceof Section\Program &&
                $section->getName() === 'program:' . self::SUPERVISOR_PROGRAM_NAME
            ;
        })->first() ?? null;
    }

    /**
     * @return Loader
     */
    protected function getReader(): Loader
    {
        return new IniFileLoader(
            new Filesystem(
                new Local(
                    dirname(self::FILESYSTEM_ROOT)
                )
            ),
            self::SUPERVISOR_CONFIG_FILE
        );
    }

    /**
     * @return Writer
     */
    protected function getWriter(): Writer
    {
        return new IniFileWriter(
            new Filesystem(
                new Local(
                    dirname(self::FILESYSTEM_ROOT)
                )
            ),
            self::SUPERVISOR_CONFIG_FILE
        );
    }

    /**
     * @return Section\Program
     */
    protected function createProgram(): Section\Program
    {
        return new Section\Program(
            self::SUPERVISOR_PROGRAM_NAME,
            [
                'command' => '/usr/bin/php ' . base_path() . '/artisan ' . self::SUPERVISOR_PROGRAM_COMMAND,
                'numprocs' => $this->accountsCount,
                'process_name' => '%(program_name)s_%(process_num)02d',
                'stdout_logfile' => '/var/log/supervisor/' . self::SUPERVISOR_PROGRAM_NAME . '.%(process_num)02d.out.log',
                'stderr_logfile' => '/var/log/supervisor/' . self::SUPERVISOR_PROGRAM_NAME . '.%(process_num)02d.error.log',
                'directory' => base_path(),
                'autostart' => true,
                'autorestart' => true,
                'stopasgroup' => true,
                'stopsignal' => 'KILL',
            ]
        );
    }

    /**
     * Initialize supervisor RPC2 connection
     */
    protected function supervisorConnection()
    {
        //Create GuzzleHttp client
        $guzzleClient = new \GuzzleHttp\Client([
            'auth' => [
                env('SUPERVISOR_USER'),
                env('SUPERVISOR_PASS'),
            ]
        ]);

        $this->supervisorInstance = new Supervisor(
            new XmlRpc(
                new Client(
                    env('SUPERVISOR_RPC2_URL'),
                    new HttpAdapterTransport(
                        new MessageFactory(),
                        new GuzzleAdapter(
                            $guzzleClient
                        )
                    )
                )
            )
        );
    }
}
