<?php

namespace App\Console\Commands;


use App\Profile;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Schema;

class StatisticCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ig:stat {type}  {--by=} {--limit=} {--from=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $type = $this->argument('type');

        if ($type == 'general') {
            $this->general();
        }

        if ($type == 'top') {
            $this->top();
        }

    }

    public function general()
    {
        $check = Profile::whereIn('status', [Profile::STATUS_CHECK, Profile::STATUS_CHECK_WITH_FOLLOWERS])
            ->count();

        $private = Profile::where('is_private', 1)
            ->count();

        $notFound = Profile::where('status', Profile::STATUS_USER_NOT_FOUND)
            ->count();

        $parentFollw = Profile::where('parent_id', '!=', null)
            ->count();

        $likeAverage = Profile::where('like_average', '!=', null)->count();

        $notPosts = Profile::where('like_average', null)
            ->where('status', Profile::STATUS_CHECK )
            ->count();

        $notPosts += Profile::where('like_average', null)
            ->where('status', Profile::STATUS_CHECK_WITH_FOLLOWERS )
            ->count();

        $notPosts -= $private;

        $total = Profile::count();

        $this->table(
            ['check', 'private', 'profile not found', 'received followers', 'profile with likes', 'haven\'t posts', 'total'],
            [[ $check, $private, $notFound, $parentFollw, $likeAverage, $notPosts, $total ]]
        );

    }

    public function top()
    {
        $type = $this->option('by');

        if (isset($type) && $type == 'followers') {
            $this->topFollowers();
        }

        if (isset($type) && $type == 'likes') {
            $this->topLikes();
        }
    }

    public function topFollowers()
    {
        $data = null;

        $headers = Schema::getColumnListing('profiles');
        $headers = array_diff($headers, array("updated_at"));

        $limit = 10;

        $limitOption = $this->option('limit');

        $from = $this->option('from');


        if (isset($limitOption)) {
            $limit = $limitOption;
        }

        if (isset($from)) {
            /**
             * @var Profile $data
             */
            $data = Profile::whereDate('created_at', '>=', $from)
                ->where('followers', '!=', null)
                ->orderBy('followers', 'desc')
                ->limit($limit)
                ->get();
        } else {
            /**
             * @var Profile $data
             */
            $data = Profile::where('followers', '!=', null)
                ->orderBy('followers', 'desc')
                ->limit($limit)
                ->get();
        }

        $data->makeHidden(['updated_at']);

        $this->table([$headers], $data->toArray());
    }

    public function topLikes()
    {
        $headers = Schema::getColumnListing('profiles');

        $headers = array_diff($headers, array("updated_at"));

        $limit = 10;

        $limitOption = $this->option('limit');

        $from = $this->option('from');


        if (isset($limitOption)) {
            $limit = $limitOption;
        }

        if (isset($from)) {
            /**
             * @var Profile $data
             */
            $data = Profile::whereDate('created_at', '>=', $from)
                ->where('average_engagement', '!=', null)
                ->orderBy('average_engagement', 'desc')
                ->limit($limit)
                ->get();
        } else {
            /**
             * @var Profile $data
             */
            $data = Profile::where('average_engagement', '!=', null)
                ->orderBy('average_engagement', 'desc')
                ->limit($limit)
                ->get();

        }

        $data->makeHidden(['updated_at']);

        $this->table([$headers], $data->toArray());
    }
}
